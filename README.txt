Requirements:
* Ubuntu (tested on 15.10)
* Java 1.8
* ansible 2.0.0.2


Automated testing:
# Deploy postgres for integration test environment
ansible-playbook -i ansible/inventory/integration ansible/postgres.yml --ask-sudo-pass

# Build and run all the tests (including integration tests)
./gradlew clean build


Deployment:
# Build the application (if you haven't already done it)
./gradlew build -x test

# Deploy the application (this will install it into ~/deploy/twino)
ansible-playbook -i ansible/inventory/local ansible/all.yml --ask-sudo-pass

# Move to the deployment directory
cd ~/deploy/twino/

# Launch the service
java -jar twino-1.0.jar


Usage:
# Applying for loan:
curl --request POST -H "Content-Type: application/json" --data '{"amount": 10.3, "term": 7, "name": "Name1", "surname": "Surname1", "personalId": "123456-789abc"}'  http://localhost:8080/loans

#Approving the loan
Approving the loan was not required according to the task, but it was still implemented
for testing purposes. Otherwise you would have to change data in the database directly.
Use the following command to approve the loan request (change id if required):
curl --request PUT -H "Content-Type: application/json" --data '{"id":1,"status":"APPROVED"}'  http://localhost:8080/loans

#Add person to blacklist
Another feature, that was not requested, but again - might be easier to use than going
directly to the database
curl --request POST --data '{"personalId": "1111-222"}' -H "Content-Type: application/json" http://localhost:8080/blacklist

# Get approved loan applications
curl --request GET http://localhost:8080/loans

# Get approved loans for specified personal id
curl --request GET http://localhost:8080/loans?personalId=123456-789abc
