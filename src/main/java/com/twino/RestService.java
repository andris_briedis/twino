package com.twino;

import com.twino.blacklist.BlacklistedPerson;
import com.twino.blacklist.BlacklistedPersonRepository;
import com.twino.geoip.GeoIp;
import com.twino.loans.*;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.LinkedList;
import java.util.List;

@Controller
public class RestService {
    @Autowired
    LoanRepository loans;

    @Autowired
    BlacklistedPersonRepository blacklist;

    @Autowired
    GeoIp geoIp;

    @Autowired
    private RequestCountLimiter requestCountLimiter;

    private Logger log = LoggerFactory.getLogger(RestService.class);

    @RequestMapping(value = "/loans", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    ResponseEntity<Loan> applyForLoan(HttpServletRequest request, @RequestBody LoanRequest loanRequest) {
        log.info("Received new post request from {}", request.getRemoteAddr());

        String country = geoIp.getCountryCode(request.getRemoteAddr());
        Loan loan;

        try {
            loan = loanRequest.toLoan(country);
        } catch (IllegalArgumentException e) {
            log.error("Invalid request: {}", e.getMessage());
            return new ResponseEntity<Loan>(HttpStatus.BAD_REQUEST);
        }

        Assert.notNull(loan, "Loan must be created if exception was not thrown");

        if (blacklist.exists(loan.getPersonalId())) {
            log.error("Not accepting loan request - person {} is in the blacklist", loan.getPersonalId());
            return new ResponseEntity<Loan>(HttpStatus.FORBIDDEN);
        }

        if (requestCountLimiter.newRequestAllowed(country)) {
            loans.save(loan);
            requestCountLimiter.addRequest(country);
        }
        else {
            log.error("Rejecting request - too many from the same country");
            return new ResponseEntity<Loan>(HttpStatus.TOO_MANY_REQUESTS);
        }

        return new ResponseEntity<Loan>(loan, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/loans", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    ResponseEntity<List<Loan>> getLoans(@RequestParam(value = "personalId", required = false) String personalId) {
        log.info("Received new get request");

        List<Loan> allLoans = new LinkedList<Loan>();

        if (personalId == null) {
            loans.findByStatus(Loan.Status.APPROVED).forEach(allLoans::add);
        }
        else {
            loans.findByStatusAndPersonalId(Loan.Status.APPROVED, personalId).forEach(allLoans::add);
        }

        return new ResponseEntity<List<Loan>>(allLoans, HttpStatus.OK);
    }

    @RequestMapping(value = "/loans", method = RequestMethod.PUT, produces = "application/json")
    @ResponseBody
    ResponseEntity<Loan> updateLoan(@RequestBody LoanApproveRequest request) {
        log.info("Updating request");
        Loan loan = loans.findOne(request.getId());
        if (loan != null) {
            loan.setStatus(request.getStatus());
            loans.save(loan);
            return new ResponseEntity<Loan>(loan, HttpStatus.OK);
        }
        else {
            log.error("Loan with id {} not found", request.getId());
            return new ResponseEntity<Loan>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/blacklist", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    ResponseEntity<BlacklistedPerson> addToBlacklist(@RequestBody BlacklistedPerson person) {
        log.info("Adding person to blacklist");
        blacklist.save(person);
        return new ResponseEntity<BlacklistedPerson>(person, HttpStatus.OK);
    }
}
