package com.twino.blacklist;

import org.springframework.data.repository.CrudRepository;

public interface BlacklistedPersonRepository extends CrudRepository<BlacklistedPerson, String> {
}
