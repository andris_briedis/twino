package com.twino.blacklist;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class BlacklistedPerson {
    @Id
    private String personalId;

    public BlacklistedPerson() {
    }

    public void setPersonalId(String personalId) {
        this.personalId = personalId;
    }

    public String getPersonalId() {
        return personalId;
    }
}
