package com.twino;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;

@SpringBootApplication
public class TwinoApp {
    public static void main(String[] args) {
        SpringApplication.run(TwinoApp.class, args);
    }
}
