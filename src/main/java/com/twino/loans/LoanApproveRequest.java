package com.twino.loans;

public class LoanApproveRequest {
    long id;
    Loan.Status status;

    public LoanApproveRequest() {

    }

    public Loan.Status getStatus() {
        return status;
    }

    public void setStatus(Loan.Status status) {
        this.status = status;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

}
