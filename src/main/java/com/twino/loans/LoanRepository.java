package com.twino.loans;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface LoanRepository extends CrudRepository<Loan, Long> {
    List<Loan> findByStatus(Loan.Status status);
    List<Loan> findByStatusAndPersonalId(Loan.Status status, String personalId);
}

