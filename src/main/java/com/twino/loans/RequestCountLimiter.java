package com.twino.loans;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.function.LongSupplier;

@Component
public class RequestCountLimiter {
    private LongSupplier currentTime;
    private long currentSec = 0;
    @Value("${request.limit ?: 100}")
    private int maxRequests;
    private Map<String, Long> requests = new HashMap<String, Long>();

    public RequestCountLimiter() {
        currentTime = System::currentTimeMillis;
    }

    public RequestCountLimiter(LongSupplier currentTime) {
        this.currentTime = currentTime;
    }

    public void addRequest(String country) {
        clearOldRequests();
        Long numRequests = requests.getOrDefault(country, 0L);
        requests.put(country, numRequests + 1);
    }

    public boolean newRequestAllowed(String country) {
        clearOldRequests();
        Long numRequests = requests.getOrDefault(country, 0L);
        return numRequests < maxRequests;
    }

    private void clearOldRequests() {
        long now = currentTime.getAsLong() / 1000;
        if (now != currentSec) {
            requests.clear();
            currentSec = now;
        }
    }
}
