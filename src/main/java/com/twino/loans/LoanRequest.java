package com.twino.loans;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.TimeZone;

public class LoanRequest {
    private static BigDecimal MIN_AMOUNT = new BigDecimal("0.01");
    private static int MIN_TERM = 1;
    private Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));

    private BigDecimal amount;
    private int term;
    private String name;
    private String surname;
    private String personalId;

    public String getPersonalId() {
        return personalId;
    }

    public String getSurname() {
        return surname;
    }

    public String getName() {
        return name;
    }

    public int getTerm() {
        return term;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Loan toLoan(String countryCode) throws IllegalArgumentException {
        validate();

        Loan loan = new Loan();
        loan.setId(0);
        loan.setAmount(amount);
        loan.setTerm(term);
        loan.setName(name);
        loan.setSurname(surname);
        loan.setPersonalId(personalId);
        loan.setStatus(Loan.Status.PENDING);
        loan.setCountryCode(countryCode);
        loan.setCreateDate(calendar.getTime());
        return loan;
    }


    private void validate() {
        if (amount == null) {
            throw new IllegalArgumentException("Amount not provided");
        }

        if (amount.compareTo(MIN_AMOUNT) == -1) {
            throw new IllegalArgumentException("Minimal amount is " + MIN_AMOUNT);
        }

        if (term < MIN_TERM) {
            throw new IllegalArgumentException("Minimal term is " + MIN_TERM);
        }

        if (name == null || name.isEmpty()) {
            throw new IllegalArgumentException("Name is not provided");
        }

        if (surname == null || surname.isEmpty()) {
            throw new IllegalArgumentException("Surname is not provided");
        }

        if (personalId == null || personalId.isEmpty()) {
            throw new IllegalArgumentException("Personal id is not provided");
        }
    }
}
