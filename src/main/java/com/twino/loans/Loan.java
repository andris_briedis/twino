package com.twino.loans;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
public class Loan {
    public enum Status {
        PENDING,
        APPROVED,
        REJECTED
    }
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column(nullable = false)
    private BigDecimal amount;
    @Column(nullable = false)
    private int term;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private String surname;
    @Column(nullable = false)
    private String personalId;
    @Column(nullable = false)
    private Status status;
    @Column(nullable = false)
    private String countryCode;
    @Column(nullable = false)
    private Date createDate;

    protected Loan() {
    }

    public void setId(long id) {
        this.id = id;
    }
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
    public void setTerm(int term) {
        this.term = term;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public void setPersonalId(String personalId) {
        this.personalId = personalId;
    }
    public void setStatus(Status status) {
        this.status = status;
    }
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public long getId() {
        return id;
    }
    public BigDecimal getAmount() {
        return amount;
    }
    public int getTerm() {
        return term;
    }
    public String getName() {
        return name;
    }
    public String getSurname() {
        return surname;
    }
    public String getPersonalId() {
        return personalId;
    }
    public Status getStatus() {
        return status;
    }
    public String getCountryCode() {
        return countryCode;
    }
    public Date getCreateDate() {
        return createDate;
    }
}
