package com.twino.geoip;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@Component
public class GeoIp {
    private static String SERVICE_URL = "http://freegeoip.net/json/{ip}";
    private static String DEFAULT_COUNTRY = "LV";

    @Autowired
    private RestTemplate restTemplate;

    private Logger log = LoggerFactory.getLogger(GeoIp.class);

    public GeoIp() {
    }

    public String getCountryCode(String ip) {
        String country;
        try {
            GeoIpResponse response = restTemplate.getForObject(SERVICE_URL, GeoIpResponse.class, ip);
            country = response.getCountryCode();
        }
        catch (RestClientException e) {
            log.error("Error getting country for ip: '{}': {}", ip, e.getMessage());
            return DEFAULT_COUNTRY;
        }

        if (country != null && country != "") {
            return country;
        }
        else {
            log.error("Country detection failed. Returning default");
            return DEFAULT_COUNTRY;
        }
    }
}
