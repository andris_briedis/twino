package com.twino.spring;

import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestConfig {
    @Value("${rest.timeout.connect ?: 1000}")
    int connectTimeout;

    @Value("${rest.timeout.read ?: 5000}")
    int readTimeout;

    Logger log = LoggerFactory.getLogger(RestConfig.class);

    @Bean
    RestTemplate restTemplate() {
        RestTemplate template = new RestTemplate();
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory(HttpClients.createDefault());
        factory.setConnectTimeout(connectTimeout);
        factory.setReadTimeout(readTimeout);
        template.setRequestFactory(factory);
        return template;
    }
}
