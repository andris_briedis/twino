import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.twino.TwinoApp
import com.twino.blacklist.BlacklistedPerson
import com.twino.blacklist.BlacklistedPersonRepository
import com.twino.loans.Loan
import com.twino.loans.LoanRepository
import groovy.json.JsonOutput
import groovyx.net.http.RESTClient
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.SpringApplicationConfiguration
import org.springframework.boot.test.WebIntegrationTest
import org.springframework.http.HttpStatus
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.TestPropertySource
import spock.lang.Specification

@SpringApplicationConfiguration(TwinoApp.class)
@WebIntegrationTest("server.port:9000")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@TestPropertySource(properties = [
        "spring.datasource.url: jdbc:postgresql://localhost:5432/test_twino",
        "spring.datasource.username: twino_user",
        "spring.datasource.password: notsosecret",
        "spring.jpa.hibernate.ddl-auto: create",
        "rest.timeout.connect: 1",
        "rest.timeout.read: 1"
])
class RestApiIntegrationTest extends Specification {
    @Autowired
    LoanRepository loanRepository;

    @Autowired
    BlacklistedPersonRepository blacklistedPersonRepository;

    def getMapper() {
        def mapper = new ObjectMapper()
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        return mapper
    }
    def rest(path = "") {
        def client = new RESTClient("http://localhost:9000/${path}")
        client.handler.failure = client.handler.success
        return client
    }

    def responseToLoan(response) {
        def responseBody = JsonOutput.toJson(response.getData())
        def loan = getMapper().readValue(responseBody, Loan.class)
        return loan
    }

    def responseToLoanList(response) {
        def responseBody = JsonOutput.toJson(response.getData())
        def loans = getMapper().readValue(responseBody, Loan[].class)
        return loans
    }

    def "Should create empty list if no loans have been created"() {
        given:
        Map<String, String> args = new HashMap<String, String>()

        when:
        def result = rest("loans").get(args)
        def loans = responseToLoanList(result)

        then:
        result.status == HttpStatus.OK.value()
        loans.size() == 0
    }

    def "Registered loan should be returned via get request"() {
        given:
        def postData = [amount: 10, term: 7, name: "Name1", surname: "Surname1", personalId: "123456-789abc"]

        when:
        def result = rest("loans").post(body: postData, requestContentType: "application/json")
        def loan = responseToLoan(result)

        then:
        result.getStatus() == HttpStatus.CREATED.value()

        loan.getAmount() == 10.0
        loan.getTerm() == 7
        loan.getName() == "Name1"
        loan.getSurname() == "Surname1"
        loan.getPersonalId() == "123456-789abc"
        loan.getId() == 1L
    }

    def "Should reset id of the object"() {
        given:
        def postData = [id: 123, amount: 10, term: 7, name: "Name1", surname: "Surname1", personalId: "123456-789abc"]

        when:
        def result = rest("loans").post(body: postData, requestContentType: "application/json")
        def loan = responseToLoan(result)

        then:
        result.getStatus() == HttpStatus.CREATED.value()
        loan.getId() == 1L
    }

    def "Should return bad request if required parameter is not specified"() {
        given:
        def postData = [term: 7, name: "Name1", surname: "Surname1", personalId: "123456-789abc"]

        when:
        def result = rest("loans").post(body: postData, requestContentType: "application/json")

        then:
        result.getStatus() == HttpStatus.BAD_REQUEST.value()
    }

    def "Only approved loans should be returned via get request"() {
        given:
        def loanData1 = [amount: 10, term: 7, name: "Name1", surname: "Surname1", personalId: "123456-789abc"]
        def loanData2 = [amount: 20, term: 7, name: "Name1", surname: "Surname1", personalId: "123456-789abc"]

        def loan1 = responseToLoan(rest("loans").post(body: loanData1, requestContentType: "application/json"))
        def loan2 = responseToLoan(rest("loans").post(body: loanData2, requestContentType: "application/json"))

        loan1.setStatus(Loan.Status.APPROVED)
        loanRepository.save(loan1)

        when:
        def response = rest("loans").get(contentType: "application/json")
        def loans = responseToLoanList(response)

        then:
        loans.size() == 1
        loans[0].getId() == loan1.getId()
        loans[0].getStatus() == Loan.Status.APPROVED
        loan2.getStatus() == Loan.Status.PENDING
    }

    def "Only approved loans for specified user must be returned if user personalId is provided"() {
        given:
        def loanData1 = [amount: 10, term: 7, name: "Name1", surname: "Surname1", personalId: "123456-789abc"]
        def loanData2 = [amount: 20, term: 7, name: "Name2", surname: "Surname2", personalId: "123456-789def"]
        def loanData3 = [amount: 30, term: 7, name: "Name3", surname: "Surname3", personalId: "123456-789def"]

        def loan1 = responseToLoan(rest("loans").post(body: loanData1, requestContentType: "application/json"))
        def loan2 = responseToLoan(rest("loans").post(body: loanData2, requestContentType: "application/json"))
        def loan3Response = rest("loans").post(body: loanData3, requestContentType: "application/json")

        loan1.setStatus(Loan.Status.APPROVED)
        loanRepository.save(loan1)
        loan2.setStatus(Loan.Status.APPROVED)
        loanRepository.save(loan2)

        when:
        def response = rest("loans").get(query: [personalId: "123456-789def"], contentType: "application/json")
        def loans = responseToLoanList(response)

        then:
        loan3Response.getStatus() == HttpStatus.CREATED.value()
        loans.size() == 1
        loans[0].getId() == loan2.getId()
    }

    def "Should reject request from blacklisted person"() {
        given:
        def person = new BlacklistedPerson();
        person.setPersonalId("111-222")
        blacklistedPersonRepository.save(person);
        def loanData = [amount: 10, term: 7, name: "Name1", surname: "Surname1", personalId: "111-222"]

        when:
        def response = rest("loans").post(body: loanData, requestContentType: "application/json")
        def loans = responseToLoanList(rest("loans").get(query: [personalId: "123456-789def"], contentType: "application/json"))

        then:
        response.getStatus() == HttpStatus.FORBIDDEN.value()
        loans.size() == 0
    }
}
