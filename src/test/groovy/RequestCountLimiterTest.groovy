import com.twino.loans.RequestCountLimiter
import spock.lang.Specification

import java.util.function.LongSupplier

class RequestCountLimiterTest extends Specification {
    def now = Mock(LongSupplier)
    def limiter = new RequestCountLimiter(now)

    def setup() {
        limiter.maxRequests = 2
    }

    def "Should allow new requests when no requests have been reported"() {
        when:
        def allowed = limiter.newRequestAllowed()

        then:
        allowed == true
    }

    def "Should allow new request if limit has not been reached"() {
        given:
        limiter.addRequest("LV")

        when:
        def allowed = limiter.newRequestAllowed("LV")

        then:
        allowed == true
    }

    def "Should deny new request if limit has been reached"() {
        given:
        limiter.addRequest("LV")
        limiter.addRequest("LV")

        when:
        def allowed = limiter.newRequestAllowed("LV")

        then:
        allowed == false
    }

    def "Should have separate counter for each country"() {
        limiter.addRequest("LV")
        limiter.addRequest("LV")
        given:

        when:
        def allowed = limiter.newRequestAllowed("GB")

        then:
        allowed == true
    }

    def "Counters should be restarted every second"() {
        now.getAsLong() >>> [1000, 2000, 2000]
        limiter.addRequest("LV")
        limiter.addRequest("LV")
        given:

        when:
        def allowed = limiter.newRequestAllowed("LV")

        then:
        allowed == true
    }

    def "Requests during previous second should not cause reject in the next second"() {
        now.getAsLong() >>> [1000, 1000, 2000]
        limiter.addRequest("LV")
        limiter.addRequest("LV")
        given:

        when:
        def allowed = limiter.newRequestAllowed("LV")

        then:
        allowed == true

    }
}
