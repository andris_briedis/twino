import com.twino.geoip.GeoIp
import com.twino.geoip.GeoIpResponse
import org.springframework.web.client.RestClientException
import org.springframework.web.client.RestTemplate
import spock.lang.Specification

class GeoIpTest extends Specification {
    def restMock = Mock(RestTemplate)
    GeoIp geoIp

    def setup() {
       geoIp = new GeoIp()
       geoIp.restTemplate = restMock
    }

    def "Should return country code for valid ip request"() {
        given:
        def ip = "212.58.246.79"
        def response = new GeoIpResponse()
        response.setCountryCode("GB")
        restMock.getForObject(_, GeoIpResponse.class, ip) >> response

        when:
        def country = geoIp.getCountryCode(ip);

        then:
        country == "GB"
    }

    def "Should return default value if country cannot be resolved by the geoip service"() {
        given:
        def ip = "127.0.0.1"
        def response = new GeoIpResponse()
        response.setCountryCode("")

        restMock.getForObject(_, GeoIpResponse.class, ip) >> response

        when:
        def country = geoIp.getCountryCode(ip);

        then:
        country == "LV"
    }

    def "Should return default value if country is not provided by the geoip service"() {
        given:
        def ip = "127.0.0.1"
        def response = new GeoIpResponse()

        restMock.getForObject(_, GeoIpResponse.class, ip) >> response

        when:
        def country = geoIp.getCountryCode(ip);

        then:
        country == "LV"
    }

    def "Should return default value in case of exception"() {
        given:
        def ip = "127.0.0.1"

        restMock.getForObject(_, GeoIpResponse.class, ip) >> {throw new RestClientException("Error")}

        when:
        def country = geoIp.getCountryCode(ip);

        then:
        country == "LV"
    }
}
