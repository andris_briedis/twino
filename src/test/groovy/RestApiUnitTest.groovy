import com.twino.blacklist.BlacklistedPersonRepository
import com.twino.geoip.GeoIp
import com.twino.loans.LoanRepository
import com.twino.loans.LoanRequest
import com.twino.RestService
import com.twino.loans.RequestCountLimiter
import org.springframework.http.HttpStatus
import spock.lang.Specification

import javax.servlet.http.HttpServletRequest

class RestApiUnitTest extends Specification {
    def service = new RestService()
    def loans = Mock(LoanRepository)
    def blacklist = Mock(BlacklistedPersonRepository)
    def geoIp = Mock(GeoIp)
    def request = Mock(HttpServletRequest)
    def loan = new LoanRequest()
    def limiter = Mock(RequestCountLimiter)

    def setup() {
        service.loans = loans
        service.geoIp = geoIp
        service.blacklist = blacklist
        service.requestCountLimiter = limiter
    }

    def "Should return bad request if request is not valid"() {
        given:
        geoIp.getCountryCode(_) >> "LV"
        limiter.newRequestAllowed(_) >>> [true]

        loan.amount = amount
        loan.term = term
        loan.name = name
        loan.surname = surname
        loan.personalId = personalId

        when:
        def response = service.applyForLoan(request, loan)

        then:
        response.statusCode == statusCode
        saveCount * loans.save(_)

        where:
        amount | term | name | surname | personalId | statusCode             | saveCount
        1.0    | 7    | "N1" | "S1"    | "000-111"  | HttpStatus.CREATED     | 1
        null   | 7    | "N1" | "S1"    | "000-111"  | HttpStatus.BAD_REQUEST | 0
        -1.0   | 7    | "N1" | "S1"    | "000-111"  | HttpStatus.BAD_REQUEST | 0
        1.0    | 0    | "N1" | "S1"    | "000-111"  | HttpStatus.BAD_REQUEST | 0
        1.0    | -1   | "N1" | "S1"    | "000-111"  | HttpStatus.BAD_REQUEST | 0
        1.0    | 7    | null | "S1"    | "000-111"  | HttpStatus.BAD_REQUEST | 0
        1.0    | 7    | ""   | "S1"    | "000-111"  | HttpStatus.BAD_REQUEST | 0
        1.0    | 7    | "N1" | ""      | "000-111"  | HttpStatus.BAD_REQUEST | 0
        1.0    | 7    | "N1" | null    | "000-111"  | HttpStatus.BAD_REQUEST | 0
        1.0    | 7    | "N1" | "S1"    | ""         | HttpStatus.BAD_REQUEST | 0
        1.0    | 7    | "N1" | "S1"    | null       | HttpStatus.BAD_REQUEST | 0
    }

    def "Should reject loan application if too many requests from the same country have already been accepted"() {
        given:

        geoIp.getCountryCode(_) >>> ["LV", "LV"]
        limiter.newRequestAllowed("LV") >>> [true, false]

        loan.amount = 1.2
        loan.term = 7
        loan.name = "MyName"
        loan.surname = "MySurname"
        loan.personalId = "111-222"

        when:
        def response1 = service.applyForLoan(request, loan)
        def response2 = service.applyForLoan(request, loan)

        then:
        response1.statusCode == HttpStatus.CREATED
        response2.statusCode == HttpStatus.TOO_MANY_REQUESTS
        1 * loans.save(_)
        1 * limiter.addRequest("LV")
    }
}
